## Questions:

     - why are there CMC api's on current dashboard, but not the open source version?
     -

## TODO:

    - normalize API call methods
        - use the same variables across all APIs (account for any account reference)
    - Don't wrap calls to other webservices.  Let them go direct to save the hop.
        - /wrapping as an example.
    - Fix 7d Volume Call
    - Fix caching implementation - it does not seem to work at the moment
        Can we feed data in to database directly, vs through API calls?
