#!/bin/bash

# Read the DATABASE line from .env file
DATABASE_LINE=$(grep '^DATABASE=' .env)

# Extract the relevant parts from the DATABASE line
DB_URL=$(echo "$DATABASE_LINE" | cut -d '=' -f 2-)
DB_PASSWORD=$(echo "$DB_URL" | awk -F '[/:@]' '{print $5}')
DB_HOST=$(echo "$DB_URL" | awk -F '[/:@]' '{print $6}')
DB_PORT=$(echo "$DB_URL" | awk -F '[/:@]' '{print $7}')
DB_NAME=$(echo "$DB_URL" | awk -F '[/:@]' '{print $8}')

# Create PostgreSQL database and set password for the postgres user
sudo -u postgres psql -h "$DB_HOST" -p "$DB_PORT" -U postgres -c "CREATE DATABASE $DB_NAME;"
sudo -u postgres psql -h "$DB_HOST" -p "$DB_PORT" -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO postgres;"

echo "Password set, database created, and privileges granted successfully."
