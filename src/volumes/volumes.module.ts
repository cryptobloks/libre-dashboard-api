import { CacheModule, Module } from '@nestjs/common';
import { VolumesService } from './volumes.service';
import { VolumesController } from './volumes.controller';
import { HttpModule } from '@nestjs/axios';
import { ExchangeRatesModule } from 'src/exchange-rates/exchange-rates.module';
import { CacheModule as InternalCacheModule } from 'src/cache/cache.module';

@Module({
  imports: [
    HttpModule,
    ExchangeRatesModule,
    CacheModule.register(),
    InternalCacheModule,
  ],
  controllers: [VolumesController],
  providers: [VolumesService],
  exports: [VolumesService],
})
export class VolumesModule {}
