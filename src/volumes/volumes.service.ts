import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { CacheService } from 'src/cache/cache.service';
import { CHAIN_CONFIG } from '../core/config';
import { ExchangeRatesService } from 'src/exchange-rates/exchange-rates.service';

@Injectable()
export class VolumesService {
  constructor(
    private readonly _httpService: HttpService,
    private readonly _exchangeRatesService: ExchangeRatesService,
    private readonly _internalCacheService: CacheService,
  ) {}

  convertToUSD(quantity: string, rates: any) {
    const amount = Number(quantity.split(' ')[0]);
    const currency = quantity.split(' ')[1];
    return parseFloat(
      Number(amount * rates[currency.toUpperCase()]).toFixed(6),
    );
  }

  async getCachedVolume(start: string) {
    const [exchangeRates, volumeBTCLIB, volumeBTCUSD] = await Promise.all([
      this._exchangeRatesService.exchangeRates(),
      this._internalCacheService.getCachedTurnover(start, 'BTCLIB'),
      this._internalCacheService.getCachedTurnover(start, 'BTCUSD'),
    ]);

    const result = {
      PBTC: 0,
      PUSDT: 0,
      LIBRE: 0,
      BTCLIB: 0,
      BTCUSD: 0,
    };

    if (volumeBTCLIB) {
      result.LIBRE = Number(volumeBTCLIB.pool_2);
      result.PBTC = Number(volumeBTCLIB.pool_1);
    }
    if (volumeBTCUSD) {
      result.PUSDT = Number(volumeBTCUSD.pool_2);
      result.PBTC += Number(volumeBTCUSD.pool_1);
    }

    result.BTCUSD = result.PUSDT * 2;
    result.BTCLIB =
      result.LIBRE * exchangeRates[CHAIN_CONFIG().libre.symbol] * 2;
    return result;
  }

  async getVolume(
    start: string,
    end: string,
    allowedSymbols = ['libre', 'pusdt', 'pbtc'],
  ) {
    const exchangeRates = await this._exchangeRatesService.exchangeRates();
    let skip = 0;
    const result = {
      PBTC: 0,
      PUSDT: 0,
      LIBRE: 0,
      BTCLIB: 0,
      BTCUSD: 0,
    };
    while (true) {
      const url = `${
        CHAIN_CONFIG().hyperionAPI.url
      }/v2/history/get_actions?limit=1000&skip=${skip}&account=swap.libre&after=${start}&before=${end}&simple=true`;
      const response = await this._httpService.get(url).toPromise();
      if (
        !response.data ||
        !response.data.simple_actions ||
        response.data.simple_actions.length === 0
      ) {
        break;
      }

      if (response && response.data) {
        for (const transaction of response.data.simple_actions) {
          if (transaction.action !== 'transfer') continue;
          const { from, to, quantity, amount, memo } = transaction.data;
          if (!quantity) continue;
          const [_, symbol] = quantity.toLowerCase().split(' ');
          if (!allowedSymbols.includes(symbol)) continue;
          const { transaction_id } = transaction;
          if (transaction) {
            const filteredTransactions = response.data.simple_actions.filter(
              (c) => c.transaction_id === transaction_id,
            );
            if (filteredTransactions && filteredTransactions.length > 2)
              continue;
            if (filteredTransactions && filteredTransactions.length < 2)
              continue;

            const filteredSymbols = filteredTransactions.map((c) =>
              c.data.quantity.split(' ')[1].toLowerCase(),
            );
            const containsNonAllowedSymbol = filteredSymbols.find(
              (s) => !allowedSymbols.includes(s),
            );
            if (containsNonAllowedSymbol) continue;

            if (symbol === 'pbtc') {
              result.PBTC += amount;
            }
            if (symbol === 'libre') {
              result.LIBRE += amount;
            }
            if (symbol === 'pusdt') {
              result.PUSDT += amount;
            }
          }
        }
      }
      skip += 1000;
    }
    result.BTCUSD = result.PUSDT * 2;
    result.BTCLIB =
      result.LIBRE * exchangeRates[CHAIN_CONFIG().libre.symbol] * 2;
    return result;
  }
}
