import {
  CacheInterceptor,
  CacheTTL,
  Controller,
  Get,
  UseInterceptors,
} from '@nestjs/common';
import { DateTime } from 'luxon';
import { VolumesService } from './volumes.service';
import { CHAIN_CONFIG } from '../core/config';

@Controller(`${CHAIN_CONFIG().baseUri.server}volumes`)
export class VolumesController {
  constructor(private readonly volumesService: VolumesService) {}

  @Get('24h')
  @CacheTTL(600)
  @UseInterceptors(CacheInterceptor)
  get24hVolume() {
    const now = DateTime.local().toUTC();
    const yesterday = now.minus({ days: 1 }).toUTC();
    const prevStr = yesterday.toISO().substring(0, 16);
    const nowStr = now.toISO().substring(0, 16);
    console.log(prevStr, nowStr);
    return this.volumesService.getVolume(prevStr, nowStr);
  }

  @Get('7d')
  @CacheTTL(600)
  @UseInterceptors(CacheInterceptor)
  async get7dVolume() {
    const now = DateTime.local().toUTC();

    const promises = [];
    for (let i = 1; i <= 7; i++) {
      const endDate = now.minus({ days: i - 1 });
      const startDate = now.minus({ days: i });
      const endStr = endDate.toISO().substring(0, 16);
      const startStr = startDate.toISO().substring(0, 16);
      promises.push(this.volumesService.getCachedVolume(startStr));
    }
    const results = await Promise.all(promises);
    console.log(results);
    const stats7d = {
      PBTC: 0,
      PUSDT: 0,
      LIBRE: 0,
      BTCLIB: 0,
      BTCUSD: 0,
    };
    for (const dayResult of results) {
      stats7d.PBTC += dayResult.PBTC;
      stats7d.BTCLIB += dayResult.BTCLIB;
      stats7d.PUSDT += dayResult.PUSDT;
      stats7d.BTCUSD += dayResult.BTCUSD;
      stats7d.LIBRE += dayResult.LIBRE;
    }
    return stats7d;
  }
}
