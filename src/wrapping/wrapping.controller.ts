import { Controller, Get, Param, Query } from '@nestjs/common';
import { WrappingService } from './wrapping.service';
import { CHAIN_CONFIG } from '../core/config';

@Controller(`${CHAIN_CONFIG().baseUri.server}wrapping`)
export class WrappingController {
  constructor(private wrappingService: WrappingService) {}

  @Get('bitcoin/:account')
  async getWrappingAddresses(@Param('account') account: string) {
    const bitcoinAddress = await this.wrappingService.getBitcoinAddress(
      account,
    );
    return {
      bitcoin: bitcoinAddress,
    };
  }

  @Get('lightning/:account')
  async getInvoice(
    @Param('account') account: string,
    @Query('amount') amount: number,
  ) {
    const lightningPaymentRequest =
      await this.wrappingService.getLightningInvoice(account, amount);
    return {
      lightning: lightningPaymentRequest,
    };
  }
}
