import { Module } from '@nestjs/common';
import { WrappingService } from './wrapping.service';
import { WrappingController } from './wrapping.controller';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [HttpModule],
  providers: [WrappingService],
  controllers: [WrappingController],
})
export class WrappingModule {}
