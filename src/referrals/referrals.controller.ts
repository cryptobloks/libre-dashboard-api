import { HttpService } from '@nestjs/axios';
import { Controller, Get, Param } from '@nestjs/common';
import {
  ReferralLeaderBoardItem,
  ReferralStats,
  UserReferrees,
} from 'src/core/interfaces';
import { ReferralsService } from './referrals.service';
import { CHAIN_CONFIG } from '../core/config';

@Controller(`${CHAIN_CONFIG().baseUri.server}referrals`)
export class ReferralsController {
  constructor(private _referralsService: ReferralsService) {}

  @Get('leaderboard')
  async leaderBoard() {
    if (CHAIN_CONFIG().environment === 'testnet') {
      return [];
    }
    return this._referralsService.leaderBoard();
  }

  @Get('user/:name')
  async myReferrees(@Param('name') name: string) {
    if (CHAIN_CONFIG().environment === 'testnet') {
      return [];
    }
    return this._referralsService.myReferrees(name);
  }

  @Get('stats/:name')
  async userStats(@Param('name') name: string): Promise<ReferralStats> {
    return {
      count: 0,
      bitcoinEarning: 0,
      libreEarning: 0,
    };
  }
}
