import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { CHAIN_CONFIG } from '../core/config';

@Injectable()
export class ReferralsService {
  constructor(private _httpService: HttpService) {}

  private CORE_API_URL = CHAIN_CONFIG().coreAPI.url;

  async leaderBoard() {
    const result = await this._httpService
      .get(`${this.CORE_API_URL}/statistics/dashboard/users`)
      .toPromise();
    return result.data;
  }

  async myReferrees(name: string) {
    const result = await this._httpService
      .get(`${this.CORE_API_URL}/statistics/dashboard/users/stats/${name}`)
      .toPromise();
    return result.data;
  }
}
