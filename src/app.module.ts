import { CacheInterceptor, CacheModule, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ReferralsModule } from './referrals/referrals.module';
import { MintsModule } from './mints/mints.module';
import { StakesModule } from './stakes/stakes.module';
import { HttpModule } from '@nestjs/axios';
import { TokensModule } from './tokens/tokens.module';
import { WrappingModule } from './wrapping/wrapping.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { ConfigModule } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';
import { CacheModule as InternalCaching } from './cache/cache.module';
import { LiquidityProvidersModule } from './liquidity-providers/liquidity-providers.module';
import { FarmingModule } from './farming/farming.module';
import { ExchangeRatesModule } from './exchange-rates/exchange-rates.module';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { VolumesModule } from './volumes/volumes.module';
import { CmcModule } from './cmc/cmc.module';
import { ContributionsModule } from './contributions/contributions.module';
import { StatsModule } from './stats/stats.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    CacheModule.register(),
    TypeOrmModule.forRoot({
      type: 'postgres',
      url: process.env.DATABASE || process.env.DATABASE_URL,
      entities: [join(__dirname, '**', '*.entity.{ts,js}')],
      ssl: true,
      synchronize: true,
    }),
    ScheduleModule.forRoot(),
    HttpModule,
    ReferralsModule,
    MintsModule,
    StakesModule,
    TokensModule,
    WrappingModule,
    InternalCaching,
    LiquidityProvidersModule,
    FarmingModule,
    ExchangeRatesModule,
    VolumesModule,
    CmcModule,
    ContributionsModule,
    StatsModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_INTERCEPTOR,
      useClass: CacheInterceptor,
    },
  ],
})
export class AppModule {}
