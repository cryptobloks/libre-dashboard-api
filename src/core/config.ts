import { ConfigModule } from '@nestjs/config';
ConfigModule.forRoot();

export const CHAIN_CONFIG = () => {
  // import constants from .env file
  const environment = process.env.ENVIRONMENT;
  const chainId = process.env.CHAIN_ID;
  const serverBaseURI = process.env.SERVER_BASE_URI;
  const swaggerBaseURI = process.env.SWAGGER_BASE_URI;
  const coreAPIUrl = process.env.CORE_API_URL;
  const hyperionAPIUrl = process.env.HYPERION_API_URL;
  const hyperionAPIUrlBackup = process.env.HYPERION_API_BAK_URL;
  const hyperionAPIUrlPublic = process.env.HYPERION_API_PUBLIC_URL;
  const libreAPIUrl = process.env.LIBRE_API_URL;
  // return config based on environment
  if (environment && environment.toLowerCase() === 'testnet') {
    return {
      environment: 'testnet',
      chainId:
        chainId ||
        'b64646740308df2ee06c6b72f34c0f7fa066d940e831f752db2006fcc2b78dee',
      baseUri: {
        server: serverBaseURI || '',
        swagger: swaggerBaseURI || '',
      },
      coreAPI: {
        url: coreAPIUrl || 'https://server.staging.bitcoinlibre.io',
      },
      hyperionAPI: {
        publicUrl: hyperionAPIUrlPublic || 'https://testnet.libre.org',
        url: hyperionAPIUrl,
        swapContract: 'evotest',
        backup_url: hyperionAPIUrlBackup || hyperionAPIUrl,
      },
      libreAPI: {
        url: libreAPIUrl,
      },
      btc: {
        symbol: 'BTCL',
        contract: 'eosio.token',
        precision: 8,
      },
      usdt: {
        symbol: 'USDL',
        contract: 'eosio.token',
        precision: 8,
      },
      libre: {
        symbol: 'LIBRE',
        contract: 'eosio.token',
        precision: 4,
      },
      btcusd: {
        precision: 7,
      },
      daofund: {
        stakeDays: 1,
      },
    };
  }
  return {
    chainId:
      chainId ||
      '38b1d7815474d0c60683ecbea321d723e83f5da6ae5f1c1f9fecc69d9ba96465',
    baseUri: {
      server: serverBaseURI || '',
      swagger: swaggerBaseURI || '',
    },
    coreAPI: {
      url: coreAPIUrl || 'https://server.production.bitcoinlibre.io',
    },
    hyperionAPI: {
      publicUrl: hyperionAPIUrlPublic || 'https://lb.libre.org',
      url: hyperionAPIUrl,
      swapContract: 'swap.libre',
      backup_url: hyperionAPIUrlBackup || hyperionAPIUrl,
    },
    libreAPI: {
      url: libreAPIUrl,
    },
    btc: {
      symbol: 'PBTC',
      contract: 'btc.ptokens',
      precision: 9,
    },
    usdt: {
      symbol: 'PUSDT',
      contract: 'usdt.ptokens',
      precision: 9,
    },
    libre: {
      symbol: 'LIBRE',
      contract: 'eosio.token',
      precision: 4,
    },
    btcusd: {
      precision: 9,
    },
    daofund: {
      stakeDays: 60,
    },
  };
};
