export interface IMintStats {
  sats: number;
  libre: number;
  rushDay: number;
  multiplier: number;
  multiplierTomorrow: number;
}

export interface IUserMintStats {
  sats: number;
  totalPercentage: number;
  availableSats: number;
}

export interface IUserContributions {
  date: Date;
  sats: number;
  percentage: number;
  multiplier: number;
  mintDate: Date;
}

export interface StakeStats {
  stakedLibre: number;
  myLibre: number;
  libreEarned: number;
}

export interface IUserStakes {
  date: Date;
  staked: number;
  apy: number;
  payout: number;
  isClaimable: number;
}

export interface ReferralStats {
  count: number;
  bitcoinEarning: number;
  libreEarning: number;
}

export interface UserReferrees {
  rank: number;
  name: string;
  earnings: number;
}

export interface ReferralLeaderBoardItem {
  rank: number;
  name: string;
  referrees: number;
}

export interface AccountStats {
  accounts: number;
  active: number;
  newAccounts: number;
}
