import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { DateTime } from 'luxon';
import { CacheService } from './cache/cache.service';
import locationMap from './core/locationMap';
import { ExchangeRatesService } from './exchange-rates/exchange-rates.service';
import { TokensService } from './tokens/tokens.service';
import { VolumesService } from './volumes/volumes.service';
import { CHAIN_CONFIG } from './core/config';

@Injectable()
export class AppService {
  constructor(
    private readonly _httpService: HttpService,
    private exchangeRatesService: ExchangeRatesService,
    private volumesService: VolumesService,
    private tokensService: TokensService,
    private internalCacheService: CacheService,
  ) {}

  get24HourVolume(symbols: string[]) {
    const now = DateTime.local().toUTC();
    const yesterday = now.minus({ days: 1 }).toUTC();
    const prevStr = yesterday.toISO().substring(0, 16);
    const nowStr = now.toISO().substring(0, 16);
    console.log(prevStr, nowStr);
    return this.volumesService.getVolume(prevStr, nowStr, symbols);
  }

  getPoolAsOrderbook(pool: string) {
    return this.tokensService.getPoolStats(pool);
  }

  getExchangeRates() {
    return this.exchangeRatesService.exchangeRates();
  }

  getLibreSatPrice() {
    return this.tokensService.getLibreBTCRate();
  }

  async getLibreStats() {
    const statsResponse = await this._httpService
      .get(`${CHAIN_CONFIG().libreAPI.url}/tokens`)
      .toPromise();
    const data = statsResponse.data.find((c) => c.name === 'LIBRE');
    console.log(data);
    const { name, supply, marketCap } = data;
    return supply;
  }

  async getChainEndpoints() {
    const chainInfo = await this._httpService
      .get(`${CHAIN_CONFIG().hyperionAPI.url}/v1/chain/get_info`)
      .toPromise();
    const producerData = await this._httpService
      .post(`${CHAIN_CONFIG().hyperionAPI.url}/v1/chain/get_table_rows`, {
        json: true,
        code: 'producerjson',
        scope: 'producerjson',
        table: 'producerjson',
        reverse: false,
        limit: -1,
      })
      .toPromise();
    const chainId = chainInfo.data.chain_id;
    const expireDate = DateTime.local().plus({ hours: 24 }).toUTC().toISO();
    const nodeList = [];

    // iterate over the producerjson table nodes list to identify all known node types
    // skip node types "producer" or "seed" as they are not relevant to the endpoints
    const nodeTypes = new Set();
    for (const item of producerData.data.rows) {
      const { json } = item;
      const { nodes } = JSON.parse(item.json);
      for (const node of nodes) {
        if (node.node_type !== 'producer') {
          nodeTypes.add(node.node_type);
        }
      }
    }

    // iterate over the producerjson table nodes list to identify all known nodes with a node_type in the nodeTypes set
    // constuct the nodesList array for each node type
    for (const nodeTypeList of nodeTypes) {
      const nodesList = [];
      for (const item of producerData.data.rows) {
        const { owner, json } = item;
        const { org, nodes } = JSON.parse(json);
        for (const node of nodes) {
          if (node.node_type === nodeTypeList) {
            nodesList.push({
              provider: owner,
              branding: org.branding,
              website: org.website,
              p2pUrl: node.p2p_endpoint,
              serviceUrl: node.api_endpoint,
              serviceUrlSecure: node.ssl_endpoint,
              features: node.features,
            });
          }
        }
      }
      nodeList.push({
        type: nodeTypeList,
        nodes: nodesList,
      });
    }

    // construct the result object
    const result = {
      chainId: chainId,
      expiration: expireDate,
      availableServiceTypes: Array.from(nodeTypes),
      service: nodeList,
    };

    // return the result
    return result;
  }

  async getProducers() {
    const data = await this._httpService
      .post(`${CHAIN_CONFIG().hyperionAPI.url}/v1/chain/get_producers`, {
        json: true,
      })
      .toPromise();
    const result = [];
    const totalVotes = Number(data.data.total_producer_vote_weight);
    let cnt = 0;
    for (const item of data.data.rows) {
      const { owner, total_votes, location, url } = item;
      const totalVotesNum = Number(total_votes);
      const percentage = (
        ((totalVotesNum * 1.0) / totalVotes) *
        1.0 *
        100
      ).toFixed(2);
      cnt++;
      result.push({
        url,
        rank: cnt,
        name: owner,
        location: locationMap.get(location),
        totalVotes: totalVotesNum,
        percentage: percentage,
      });
    }
    return result;
  }

  async getVotee(account: string) {
    const result = await this._httpService
      .post(`${CHAIN_CONFIG().hyperionAPI.url}/v1/chain/get_table_rows`, {
        code: 'eosio',
        table: 'voters',
        scope: 'eosio',
        json: true,
        lower_bound: account,
        upper_bound: account,
      })
      .toPromise();
    const rows = result.data.rows;

    if (rows.length === 0) {
      return {
        votedFor: null,
      };
    } else {
      return {
        votedFor: rows[0].producers[0],
      };
    }
  }

  async getChainSettings() {
    return {
      chainId: CHAIN_CONFIG().chainId,
      hyperionUrl: CHAIN_CONFIG().hyperionAPI.publicUrl,
    };
  }

  async getChainStats() {
    try {
      const data = await this._httpService
        .get(`${CHAIN_CONFIG().coreAPI.url}/statistics/dashboard/app`)
        .toPromise();
      return data.data;
    } catch (e) {
      return {
        accounts: 0,
        active: 0,
        newAccounts: 0,
        referrals: 0,
      };
    }
  }

  async calculateTurnover(date: string) {
    console.log(`Calculating turnover: ${date}`);
    const dateStart = DateTime.fromISO(date).toUTC().toISODate();
    const dateEnd = DateTime.fromISO(date)
      .plus({ days: 1 })
      .toUTC()
      .toISODate();
    const btcUSD = await this.volumesService.getVolume(dateStart, dateEnd, [
      'pbtc',
      'pusdt',
    ]);
    console.log(btcUSD);
    const btcLIB = await this.volumesService.getVolume(dateStart, dateEnd, [
      'pbtc',
      'libre',
    ]);
    await this.internalCacheService.insertTurnover(
      date,
      'BTCLIB',
      btcLIB.PBTC,
      btcLIB.LIBRE,
    );
    const saved = await this.internalCacheService.insertTurnover(
      date,
      'BTCUSD',
      btcUSD.PBTC,
      btcUSD.PUSDT,
    );
    console.log(saved);
  }
}
