import { Controller, Get, Param } from '@nestjs/common';
import { MintsService } from './mints.service';
import { CHAIN_CONFIG } from '../core/config';

@Controller(`${CHAIN_CONFIG().baseUri.server}mints`)
export class MintsController {
  constructor(private readonly _mintService: MintsService) {}

  @Get('stats')
  async getMintStats() {
    const mintStats = await this._mintService.findMintStats();

    return mintStats;
  }

  @Get(':account/list')
  findByAccount(@Param('account') account: string) {
    return this._mintService.findByAccount(account);
  }

  @Get(':account/stats')
  async getMintData(@Param('account') account: string) {
    const mintsByAccount = await this._mintService.findByAccount(account);
    const contributed = mintsByAccount
      .map((c) =>
        Number(c.btc_contributed.replace(CHAIN_CONFIG().btc.symbol, '')),
      )
      .reduce((a, b) => a + b, 0);

    const libreContributed = mintsByAccount
      .map((c) =>
        Number(c.libre_minted.replace(CHAIN_CONFIG().libre.symbol, '')),
      )
      .reduce((a, b) => a + b, 0);

    const mintStats = await this._mintService.findMintStats();

    return {
      contributed,
      libre_contributed: libreContributed,
      percentage: (100 * contributed) / mintStats.btc_contributed,
    };
  }
}
