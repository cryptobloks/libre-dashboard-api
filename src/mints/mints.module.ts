import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { MintsController } from './mints.controller';
import { MintsService } from './mints.service';

@Module({
  imports: [HttpModule],
  controllers: [MintsController],
  providers: [MintsService],
  exports: [MintsService],
})
export class MintsModule {}
