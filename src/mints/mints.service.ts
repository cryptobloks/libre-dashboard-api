import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { CHAIN_CONFIG } from '../core/config';

@Injectable()
export class MintsService {
  constructor(private readonly _httpService: HttpService) {}

  async findAll() {
    const data = [];
    let cursor = 0;
    const limit = 10;

    while (true) {
      const result = await this._httpService
        .post(`${CHAIN_CONFIG().hyperionAPI.url}/v1/chain/get_table_rows`, {
          code: 'stake.libre',
          table: 'mintbonus',
          scope: 'stake.libre',
          json: true,
          lower_bound: cursor,
          upper_bound: cursor + limit,
        })
        .toPromise();
      const rows = result.data.rows;
      data.push(...rows);
      cursor += limit;
      if (rows.length === 0) {
        break;
      }
    }
    return data;
  }

  async findByAccount(account: string) {
    const resultStats = await this.findMintStats();
    const result = await this._httpService
      .post(`${CHAIN_CONFIG().hyperionAPI.url}/v1/chain/get_table_rows`, {
        code: 'stake.libre',
        table: 'mintbonus',
        scope: 'stake.libre',
        json: true,
        index_position: 2,
        upper_bound: account,
        key_type: 'name',
        limit: 1000,
      })
      .toPromise();
    let data = result.data.rows;
    data = data.map((c) => {
      const mintRushDay = c.day_of_mint_rush;
      const dayMs =
        new Date(resultStats.start_date).getTime() + mintRushDay * 86400000;

      return {
        contribution_date: new Date(dayMs),
        ...c,
      };
    });
    data = data.filter((row) => row.account === account);
    return data;
  }

  async findMintStats() {
    const result = await this._httpService
      .post(`${CHAIN_CONFIG().hyperionAPI.url}/v1/chain/get_table_rows`, {
        code: 'stake.libre',
        table: 'mintstats',
        scope: 'stake.libre',
        json: true,
      })
      .toPromise();
    // current mint day HARDCODED on backend
    const mintStartDate = new Date(result.data.rows[0].start_date);
    const nowDate = new Date();
    const diff = Math.abs(mintStartDate.getTime() - nowDate.getTime());
    console.log(diff);
    const diffDays = Math.floor(diff / (1000 * 3600 * 24));
    console.log(diffDays);
    // mint bonus today
    const mintBonusToday = 1 - diffDays / 30;
    // mint bonus tomorrow
    const mintBonusTomorrow = 1 - (diffDays + 1) / 30;
    return {
      start_date: result.data.rows[0].start_date,
      end_date: result.data.rows[0].end_date,
      btc_contributed: result.data.rows[0].btc_contributed.split(
        ` ${CHAIN_CONFIG().btc.symbol.toUpperCase()}`,
      )[0] as number,
      mint_rush_day: diffDays + 1,
      mint_bonus_today: mintBonusToday,
      mint_bonus_tomorrow: mintBonusTomorrow,
    };
  }
}
