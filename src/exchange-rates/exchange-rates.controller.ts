import {
  CacheInterceptor,
  CacheTTL,
  Controller,
  Get,
  UseInterceptors,
} from '@nestjs/common';
import { ExchangeRatesService } from './exchange-rates.service';
import { CHAIN_CONFIG } from '../core/config';

@Controller(`${CHAIN_CONFIG().baseUri.server}exchange-rates`)
export class ExchangeRatesController {
  constructor(private readonly exchangeRatesService: ExchangeRatesService) {}

  @Get()
  @CacheTTL(10)
  @UseInterceptors(CacheInterceptor)
  getRates() {
    return this.exchangeRatesService.exchangeRates();
  }
}
