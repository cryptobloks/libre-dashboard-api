import { Injectable } from '@nestjs/common';
import { FarmingService } from 'src/farming/farming.service';
import { TokensService } from 'src/tokens/tokens.service';
import { CHAIN_CONFIG } from '../core/config';

@Injectable()
export class ExchangeRatesService {
  constructor(
    private readonly tokensService: TokensService,
    private readonly farmingService: FarmingService,
  ) {}

  async exchangeRates() {
    const [librePrice, libreSatPrice, btcPrice, btcLIBREStats, btcUSDStats] =
      await Promise.all([
        this.tokensService.getLibrePrice(),
        this.tokensService.getLibreBTCRate(),
        this.tokensService.getBTCUSDRate(),
        this.farmingService.getBTCLIBFarmingStats(),
        this.farmingService.getBTCUSDFarmingStats(),
      ]);
    const rates: any = {};

    rates[CHAIN_CONFIG().btc.symbol] = btcPrice;
    rates[CHAIN_CONFIG().libre.symbol] = librePrice;
    rates[CHAIN_CONFIG().usdt.symbol] = 1;
    rates['BTCLIB'] = btcLIBREStats.lpTokenValue;
    rates['BTCUSD'] = btcUSDStats.lpTokenValue;
    rates['BTCSAT'] = libreSatPrice;

    return rates;
  }
}
