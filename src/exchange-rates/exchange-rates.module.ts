import { CacheModule, Module } from '@nestjs/common';
import { ExchangeRatesService } from './exchange-rates.service';
import { ExchangeRatesController } from './exchange-rates.controller';
import { TokensModule } from 'src/tokens/tokens.module';
import { FarmingModule } from 'src/farming/farming.module';

@Module({
  imports: [TokensModule, FarmingModule, CacheModule.register()],
  providers: [ExchangeRatesService],
  controllers: [ExchangeRatesController],
  exports: [ExchangeRatesService],
})
export class ExchangeRatesModule {}
