import { Module } from '@nestjs/common';
import { CmcService } from './cmc.service';
import { CmcController } from './cmc.controller';
import { AppService } from 'src/app.service';
import { ExchangeRatesModule } from 'src/exchange-rates/exchange-rates.module';
import { TokensModule } from 'src/tokens/tokens.module';
import { VolumesModule } from 'src/volumes/volumes.module';

@Module({
  imports: [ExchangeRatesModule, TokensModule, VolumesModule],
  providers: [CmcService],
  controllers: [CmcController],
})
export class CmcModule {}
