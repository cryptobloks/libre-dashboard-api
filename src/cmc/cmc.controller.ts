import {
  CacheTTL,
  Controller,
  Get,
  NotFoundException,
  Param,
  Req,
} from '@nestjs/common';
import { AppService } from 'src/app.service';
import { CHAIN_CONFIG } from 'src/core/config';
import { CmcService } from './cmc.service';

@Controller(`${CHAIN_CONFIG().baseUri.server}cmc/v1`)
export class CmcController {
  constructor(private readonly cmcService: CmcService) {}

  @Get('summary')
  @CacheTTL(600)
  async getTickers() {
    const [
      exchangeRates,
      libreSatPrice,
      btcUSDPool,
      btcLibPool,
      btcUSDVolume,
      btcLibreVolume,
    ] = await Promise.all([
      this.cmcService.getExchangeRates(),
      this.cmcService.getLibreSatPrice(),
      this.cmcService.getPoolAsOrderbook('BTCUSD'),
      this.cmcService.getPoolAsOrderbook('BTCLIB'),
      this.cmcService.get24HourVolume(['pbtc', 'pusdt']),
      this.cmcService.get24HourVolume(['pbtc', 'libre']),
    ]);
    const usdInBTCUSD =
      btcUSDPool.pool1Amount * exchangeRates[CHAIN_CONFIG().btc.symbol] +
      btcUSDPool.pool2Amount;
    const usdInBTCLIB =
      btcLibPool.pool1Amount * exchangeRates[CHAIN_CONFIG().btc.symbol] +
      btcLibPool.pool2Amount * exchangeRates[CHAIN_CONFIG().libre.symbol];
    const btcUSDTicker: any = {
      ticker_id: 'BTC_USDT',
      base_currency: 'BTC',
      target_currency: 'USDT',
      last_price: exchangeRates[CHAIN_CONFIG().btc.symbol],
      base_volume: Number(
        (
          btcUSDVolume.BTCUSD / exchangeRates[CHAIN_CONFIG().btc.symbol]
        ).toFixed(8),
      ),
      target_volume: Number(
        (
          btcUSDVolume.BTCUSD / exchangeRates[CHAIN_CONFIG().usdt.symbol]
        ).toFixed(8),
      ),
      liquidity_in_usd: usdInBTCUSD,
      pool_id: 'BTCUSD',
      percentChange: 0,
      highestAsk: exchangeRates[CHAIN_CONFIG().btc.symbol],
      lowestBid: exchangeRates[CHAIN_CONFIG().btc.symbol],
      high24h: exchangeRates[CHAIN_CONFIG().btc.symbol],
      low24h: exchangeRates[CHAIN_CONFIG().btc.symbol],
    };
    btcUSDTicker.total_volume = Number(btcUSDVolume.BTCUSD.toFixed(2));
    const btcLIBRETicker: any = {
      ticker_id: 'BTC_LIBRE',
      base_currency: 'BTC',
      target_currency: 'LIBRE',
      last_price: Number((1 / libreSatPrice).toFixed(2)),
      base_volume: Number(
        btcLibreVolume.BTCLIB /
          exchangeRates[CHAIN_CONFIG().btc.symbol].toFixed(8),
      ),
      target_volume: Number(
        (
          btcLibreVolume.BTCLIB / exchangeRates[CHAIN_CONFIG().libre.symbol]
        ).toFixed(8),
      ),
      liquidity_in_usd: usdInBTCLIB,
      pool_id: 'BTCLIB',
      percentChange: 0,
      highestAsk: Number((1 / libreSatPrice).toFixed(2)),
      lowestBid: Number((1 / libreSatPrice).toFixed(2)),
      high24h: Number((1 / libreSatPrice).toFixed(2)),
      low24h: Number((1 / libreSatPrice).toFixed(2)),
    };
    btcLIBRETicker.total_volume = Number(btcLibreVolume.BTCLIB.toFixed(2));
    return [btcUSDTicker, btcLIBRETicker];
  }

  @Get('assets')
  async getAssets() {
    const assets = {
      BTC: {
        name: 'bitcoin',
        unified_cryptoasset_id: '1',
        can_withdraw: 'true',
        can_deposit: 'true',
        min_withdraw: '0.002',
        max_withdraw: '100',
        maker_fee: 0,
        taker_fee: 0,
      },
      USDT: {
        name: 'Tether',
        unified_cryptoasset_id: '825',
        can_withdraw: 'true',
        can_deposit: 'true',
        min_withdraw: '10',
        max_withdraw: '10000000',
        maker_fee: 0,
        taker_fee: 0,
      },
      LIBRE: {
        name: 'Libre',
        unified_cryptoasset_id: null,
        can_withdraw: 'true',
        can_deposit: 'true',
        min_withdraw: '0.001',
        max_withdraw: '1000000000',
        maker_fee: 0,
        taker_fee: 0,
      },
    };
    return assets;
  }

  @CacheTTL(600)
  @Get('ticker')
  async getTicker() {
    const [
      exchangeRates,
      libreSatPrice,
      btcUSDPool,
      btcLibPool,
      btcUSDVolume,
      btcLibreVolume,
    ] = await Promise.all([
      this.cmcService.getExchangeRates(),
      this.cmcService.getLibreSatPrice(),
      this.cmcService.getPoolAsOrderbook('BTCUSD'),
      this.cmcService.getPoolAsOrderbook('BTCLIB'),
      this.cmcService.get24HourVolume(['pbtc', 'pusdt']),
      this.cmcService.get24HourVolume(['pbtc', 'libre']),
    ]);
    const usdInBTCUSD =
      btcUSDPool.pool1Amount * exchangeRates[CHAIN_CONFIG().btc.symbol] +
      btcUSDPool.pool2Amount;
    const usdInBTCLIB =
      btcLibPool.pool1Amount * exchangeRates[CHAIN_CONFIG().btc.symbol] +
      btcLibPool.pool2Amount * exchangeRates[CHAIN_CONFIG().libre.symbol];
    const btcUSDTicker: any = {
      ticker_id: 'BTC_USDT',
      base_currency: 'BTC',
      target_currency: 'USDT',
      last_price: exchangeRates[CHAIN_CONFIG().btc.symbol],
      base_volume: Number(
        (
          btcUSDVolume.BTCUSD / exchangeRates[CHAIN_CONFIG().btc.symbol]
        ).toFixed(8),
      ),
      target_volume: Number(
        (
          btcUSDVolume.BTCUSD / exchangeRates[CHAIN_CONFIG().usdt.symbol]
        ).toFixed(8),
      ),
      liquidity_in_usd: usdInBTCUSD,
      pool_id: 'BTCUSD',
      percentChange: 0,
      highestAsk: exchangeRates[CHAIN_CONFIG().btc.symbol],
      lowestBid: exchangeRates[CHAIN_CONFIG().btc.symbol],
      high24h: exchangeRates[CHAIN_CONFIG().btc.symbol],
      low24h: exchangeRates[CHAIN_CONFIG().btc.symbol],
    };
    btcUSDTicker.total_volume = Number(btcUSDVolume.BTCUSD.toFixed(2));
    const btcLIBRETicker: any = {
      ticker_id: 'BTC_LIBRE',
      base_currency: 'BTC',
      target_currency: 'LIBRE',
      last_price: Number((1 / libreSatPrice).toFixed(2)),
      base_volume: Number(
        btcLibreVolume.BTCLIB /
          exchangeRates[CHAIN_CONFIG().btc.symbol].toFixed(8),
      ),
      target_volume: Number(
        (
          btcLibreVolume.BTCLIB / exchangeRates[CHAIN_CONFIG().libre.symbol]
        ).toFixed(8),
      ),
      liquidity_in_usd: usdInBTCLIB,
      pool_id: 'BTCLIB',
      percentChange: 0,
      highestAsk: Number((1 / libreSatPrice).toFixed(2)),
      lowestBid: Number((1 / libreSatPrice).toFixed(2)),
      high24h: Number((1 / libreSatPrice).toFixed(2)),
      low24h: Number((1 / libreSatPrice).toFixed(2)),
    };
    btcLIBRETicker.total_volume = Number(btcLibreVolume.BTCLIB.toFixed(2));
    const response = {
      BTC_USDT: {
        base_id: 1,
        target_id: 825,
        last_price: btcUSDTicker.last_price,
        quote_volume: btcUSDTicker.target_volume,
        base_volume: btcUSDTicker.base_volume,
        isFrozen: 0,
      },
      BTC_LIBRE: {
        base_id: 1,
        target_id: 0,
        last_price: btcLIBRETicker.last_price,
        quote_volume: btcLIBRETicker.target_volume,
        base_volume: btcLIBRETicker.base_volume,
        isFrozen: 0,
      },
    };
    return response;
  }

  @Get('orderbook/:tickerId')
  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  async getOrderBook(@Param('tickerId') tickerId: string = '') {
    let data;
    if (tickerId.toLowerCase() === 'btc_usdt') {
      data = await this.cmcService.getPoolAsOrderbook('btcusd');
    }
    if (tickerId.toLowerCase() === 'btc_libre') {
      data = await this.cmcService.getPoolAsOrderbook('btclib');
    }
    if (!data) {
      throw new NotFoundException();
    }
    const response = {
      ticker_id: tickerId.toUpperCase(),
      timestamp: new Date().getTime(),
      bids: [[data.pool1Amount, data.pool2Amount]],
      asks: [[data.pool1Amount, data.pool2Amount]],
    };
    return response;
  }
}
