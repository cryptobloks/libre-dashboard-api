import { Injectable } from '@nestjs/common';
import { DateTime } from 'luxon';
import { ExchangeRatesService } from 'src/exchange-rates/exchange-rates.service';
import { TokensService } from 'src/tokens/tokens.service';
import { VolumesService } from 'src/volumes/volumes.service';

@Injectable()
export class CmcService {
  constructor(
    private exchangeRatesService: ExchangeRatesService,
    private tokensService: TokensService,
    private volumesService: VolumesService,
  ) {}

  getPoolAsOrderbook(pool: string) {
    return this.tokensService.getPoolStats(pool);
  }

  getExchangeRates() {
    return this.exchangeRatesService.exchangeRates();
  }

  getLibreSatPrice() {
    return this.tokensService.getLibreBTCRate();
  }

  get24HourVolume(symbols: string[]) {
    const now = DateTime.local().toUTC();
    const yesterday = now.minus({ days: 1 }).toUTC();
    const prevStr = yesterday.toISO().substring(0, 16);
    const nowStr = now.toISO().substring(0, 16);
    console.log(prevStr, nowStr);
    return this.volumesService.getVolume(prevStr, nowStr, symbols);
  }
}
