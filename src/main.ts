import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { CacheInterceptor } from '@nestjs/common';
import { CHAIN_CONFIG } from './core/config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();

  const options = new DocumentBuilder()
    .setTitle('Libre Api')
    .setDescription('Libre API Description')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup(`${CHAIN_CONFIG().baseUri.swagger}docs`, app, document);
  await app.listen(process.env.SERVER_PORT || 3000);
}

bootstrap();
