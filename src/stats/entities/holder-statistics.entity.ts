import { Entity, Column } from 'typeorm';
import { BaseEntity } from '../../core/entities/base.entity';

@Entity({ name: 'holder_statistics' })
export class HolderStatistics extends BaseEntity {
  constructor(partial: Partial<HolderStatistics>) {
    super();
    Object.assign(this, partial);
  }

  @Column({ type: 'varchar', length: 200, nullable: true })
  token: string;

  @Column({ type: 'json', nullable: false })
  statistics: any;
}
