import { Controller, Get, Param } from '@nestjs/common';
import { StatsService } from './stats.service';
import { CHAIN_CONFIG } from '../core/config';

@Controller(`${CHAIN_CONFIG().baseUri.server}stats`)
export class StatsController {
  constructor(private readonly _statsService: StatsService) {}

  @Get('libre/liquid')
  async getLatestStatsByToken() {
    const stats = await this._statsService.getLatestStatsByToken('LIBRE');
    if (stats && stats.length > 0) {
      return stats[0].statistics;
    } else {
      return [];
    }
  }

  @Get('libre/staker-richlist')
  async getStakerRichlist() {
    return this._statsService.getStakerRichlist();
  }

  @Get('libre/top-holders')
  async getFullStats() {
    return this._statsService.getFullHolderStats();
  }
}
