import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { HolderStatistics } from './entities/holder-statistics.entity';
import { Repository } from 'typeorm';

@Injectable()
export class StatsService {
  constructor(
    private readonly _httpService: HttpService,
    @InjectRepository(HolderStatistics)
    private readonly _holderStatsRepository: Repository<HolderStatistics>,
  ) {}

  async getLatestStatsByToken(token: string) {
    return this._holderStatsRepository.find({
      where: {
        token,
      },
      order: {
        createdAt: 'DESC',
      },
      take: 1,
    });
  }

  async getStakerRichlist() {
    const query = `
      select
          account,
          sum(cast(replace(payout, 'LIBRE', '') as numeric)) as total_staked
      from stakes
      group by account
      order by sum(cast(replace(payout, 'LIBRE', '') as numeric)) desc
    `;
    const queryResult = await this._holderStatsRepository.query(query);
    const richlist = queryResult.map((item) => {
      return {
        account: item.account,
        amount: Number(item.total_staked),
      };
    });
    return richlist;
  }

  async getStats() {
    const response = await this._httpService
      .get(
        'https://light.libre.quantumblok.com/api/topholders/libre/eosio.token/LIBRE/100?pretty=1',
      )
      .toPromise();
    // it will come in the form of an array of arrays, where first element is account name and second - amount of tokens
    // turn it into an object
    const data = [];
    for (let i = 0; i < response.data.length; i++) {
      // stake.libre is not a holder - it's a contract where staked tokens are held, so we skip it
      if (response.data[i][0] === 'stake.libre') {
        continue;
      }
      data.push({
        account: response.data[i][0],
        amount: Number(response.data[i][1]),
      });
    }
    // save stats
    const stats = this._holderStatsRepository.create({
      token: 'LIBRE',
      statistics: JSON.stringify(data),
    });
    return await this._holderStatsRepository.save(stats);
    // return data;
  }

  async getFullHolderStats() {
    const liquidStats = await this.getStats();
    const stakerStats = await this.getStakerRichlist();

    // we need to return rank, account, liquid amount, staked amount, total amount
    const result = [];
    for (const stat of JSON.parse(liquidStats.statistics)) {
      const staker = stakerStats.find((item) => item.account === stat.account);
      result.push({
        account: stat.account,
        liquid: stat.amount,
        staked: staker ? staker.amount : 0,
        total: stat.amount + (staker ? staker.amount : 0),
      });
    }
    // sort by total amount
    result.sort((a, b) => b.total - a.total);
    // add rank
    for (let i = 0; i < result.length; i++) {
      result[i].rank = i + 1;
    }
    // take only top 100
    result.splice(100);
    return result;
  }
}
