import { Module } from '@nestjs/common';
import { StatsService } from './stats.service';
import { StatsController } from './stats.controller';
import { HttpModule } from '@nestjs/axios';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HolderStatistics } from './entities/holder-statistics.entity';

@Module({
  imports: [HttpModule, TypeOrmModule.forFeature([HolderStatistics])],
  providers: [StatsService],
  controllers: [StatsController],
})
export class StatsModule {}
