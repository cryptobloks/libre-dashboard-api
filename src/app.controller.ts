import {
  CacheTTL,
  Controller,
  Get,
  NotFoundException,
  Param,
  Query,
  Req,
} from '@nestjs/common';
import { Request } from 'express';
import { AppService } from './app.service';
import { CHAIN_CONFIG } from './core/config';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}
  @Get(`${CHAIN_CONFIG().baseUri.server}producers`)
  getProducers() {
    return this.appService.getProducers();
  }

  @Get(`${CHAIN_CONFIG().baseUri.server}getChainEndpoints`)
  getEndpoints() {
    return this.appService.getChainEndpoints();
  }

  @Get(`${CHAIN_CONFIG().baseUri.server}voted/:account`)
  getVotee(@Param('account') account: string) {
    return this.appService.getVotee(account);
  }

  @Get(`${CHAIN_CONFIG().baseUri.server}chainSettings`)
  getChainSettings() {
    return this.appService.getChainSettings();
  }

  @Get(`${CHAIN_CONFIG().baseUri.server}stats/chain`)
  getChainStats() {
    return this.appService.getChainStats();
  }

  @Get(`${CHAIN_CONFIG().baseUri.server}circulating-supply`)
  getLibreStats() {
    return this.appService.getLibreStats();
  }

  @Get(`${CHAIN_CONFIG().baseUri.server}pairs`)
  getPairs() {
    const pairs = [
      {
        ticker_id: 'BTC_LIB',
        base: 'BTC',
        target: 'LIBRE',
        pool_id: 'BTCLIB',
      },
      {
        ticker_id: 'BTC_USD',
        base: 'BTC',
        target: 'USDT',
        pool_id: 'BTCUSD',
      },
    ];
    return pairs;
  }

  @Get(`${CHAIN_CONFIG().baseUri.server}tickers`)
  @CacheTTL(600)
  async getTickers(@Req() req: Request) {
    const [
      exchangeRates,
      libreSatPrice,
      btcUSDPool,
      btcLibPool,
      btcUSDVolume,
      btcLibreVolume,
    ] = await Promise.all([
      this.appService.getExchangeRates(),
      this.appService.getLibreSatPrice(),
      this.appService.getPoolAsOrderbook('BTCUSD'),
      this.appService.getPoolAsOrderbook('BTCLIB'),
      this.appService.get24HourVolume(['pbtc', 'pusdt']),
      this.appService.get24HourVolume(['pbtc', 'libre']),
    ]);
    const usdInBTCUSD =
      btcUSDPool.pool1Amount * exchangeRates[CHAIN_CONFIG().btc.symbol] +
      btcUSDPool.pool2Amount;
    const usdInBTCLIB =
      btcLibPool.pool1Amount * exchangeRates[CHAIN_CONFIG().btc.symbol] +
      btcLibPool.pool2Amount * exchangeRates[CHAIN_CONFIG().libre.symbol];
    const btcUSDTicker: any = {
      ticker_id: 'BTC_USDT',
      base_currency: 'BTC',
      target_currency: 'USDT',
      last_price: exchangeRates[CHAIN_CONFIG().btc.symbol],
      base_volume: Number(
        (
          btcUSDVolume.BTCUSD / exchangeRates[CHAIN_CONFIG().btc.symbol]
        ).toFixed(8),
      ),
      target_volume: Number(
        (
          btcUSDVolume.BTCUSD / exchangeRates[CHAIN_CONFIG().usdt.symbol]
        ).toFixed(8),
      ),
      liquidity_in_usd: usdInBTCUSD,
      pool_id: 'BTCUSD',
    };
    btcUSDTicker.total_volume = Number(btcUSDVolume.BTCUSD.toFixed(2));
    const btcLIBRETicker: any = {
      ticker_id: 'BTC_LIBRE',
      base_currency: 'BTC',
      target_currency: 'LIBRE',
      last_price: Number((1 / libreSatPrice).toFixed(2)),
      base_volume: Number(
        btcLibreVolume.BTCLIB /
          exchangeRates[CHAIN_CONFIG().btc.symbol].toFixed(8),
      ),
      target_volume: Number(
        (
          btcLibreVolume.BTCLIB / exchangeRates[CHAIN_CONFIG().libre.symbol]
        ).toFixed(8),
      ),
      liquidity_in_usd: usdInBTCLIB,
      pool_id: 'BTCLIB',
    };
    btcLIBRETicker.total_volume = Number(btcLibreVolume.BTCLIB.toFixed(2));
    return [btcUSDTicker, btcLIBRETicker];
  }

  @Get(`${CHAIN_CONFIG().baseUri.server}orderbook/:tickerId`)
  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  async getOrderBook(@Param('tickerId') tickerId: string = '') {
    let data;
    if (tickerId.toLowerCase() === 'btc_usdt') {
      data = await this.appService.getPoolAsOrderbook('btcusd');
    }
    if (tickerId.toLowerCase() === 'btc_libre') {
      data = await this.appService.getPoolAsOrderbook('btclib');
    }
    if (!data) {
      throw new NotFoundException();
    }
    const response = {
      ticker_id: tickerId.toUpperCase(),
      timestamp: new Date().getTime(),
      bids: [[data.pool1Amount, data.pool2Amount]],
      asks: [[data.pool1Amount, data.pool2Amount]],
    };
    return response;
  }

  @Get(`${CHAIN_CONFIG().baseUri.server}cacheTurnover`)
  async test(@Query('date') date: string) {
    const dates = [
      '2023-02-24',
      '2023-02-25',
      '2023-02-26',
      '2023-02-27',
      '2023-02-28',
      '2023-03-01',
    ];
    for (const x of dates) {
      await this.appService.calculateTurnover(x);
    }
    return {
      result: 'ok',
    };
  }
}
