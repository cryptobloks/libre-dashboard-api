import { Controller, Get, Param } from '@nestjs/common';
import { ContributionsService } from './contributions.service';
import { CHAIN_CONFIG } from '../core/config';

@Controller(`${CHAIN_CONFIG().baseUri.server}contributions`)
export class ContributionsController {
  constructor(private readonly contributionsService: ContributionsService) {}

  @Get()
  getGlobalData() {
    return this.contributionsService.getGlobalStats();
  }

  @Get(':account')
  getAccountData(@Param('account') account: string) {
    return this.contributionsService.getContributionsByUser(account);
  }
}
