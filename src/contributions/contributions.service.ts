import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { JsonRpc } from 'eosjs';
import { CHAIN_CONFIG } from 'src/core/config';
import { TokensService } from 'src/tokens/tokens.service';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const fetch = require('node-fetch');

@Injectable()
export class ContributionsService {
  private _rpc: JsonRpc;

  constructor(
    private _httpService: HttpService,
    private readonly _tokensService: TokensService,
  ) {
    this._rpc = new JsonRpc(CHAIN_CONFIG().hyperionAPI.url, { fetch });
  }

  public async getContributionsByUser(account: string) {
    const rows = await this.getContributionsTable();
    let myContributions = 0;
    let initialAllocation = 0;
    const totalClaimed = 0;

    const contributions = rows
      .filter((row) => row.account === account)
      .map((row) => {
        const contributedSats = this.btcToSatoshi(row.pbtc_amount);
        const allocatedLibre = this.libreStringToNumber(row.stake_amount);

        // if stake date is there, add 181 days
        const claimDate =
          row.stake_date !== '1970-01-01T00:00:00'
            ? new Date(
                new Date(row.stake_date).getTime() + 181 * 24 * 60 * 60 * 1000,
              ).getTime()
            : null;

        myContributions += contributedSats;
        initialAllocation += allocatedLibre;

        const contributionTime = new Date(row.contr_date).getTime();

        // two days after contribution
        const stakeDate =
          row.stake_date !== '1970-01-01T00:00:00'
            ? new Date(row.stake_date).getTime()
            : new Date(contributionTime + 2 * 24 * 60 * 60 * 1000).getTime();

        return {
          contributionTime,
          contributedSats,
          allocatedLibre,
          status: row.status,
          stakeDate,
          claimDate,
          stakeDays: CHAIN_CONFIG().daofund.stakeDays,
          id: row.index,
        };
      });

    return {
      myContributions,
      initialAllocation,
      totalClaimed,
      contributions,
    };
  }

  public async getGlobalStats() {
    const rows = await this.getContributionsTable();
    let contributions = 0;
    let contributedToday = 0;

    for (const row of rows) {
      contributions += this.btcToSatoshi(row.pbtc_amount);
      if (this.isToday(row.contr_date)) {
        contributedToday += this.btcToSatoshi(row.pbtc_amount);
      }
    }
    const { start_date, end_date } = await this.getContributionStats();
    return {
      totalContributed: contributions,
      currentContributed: contributedToday,
      libreValue: (contributedToday / 10000000).toFixed(4),
      currentPeriod: await this.getDaysAfter(start_date),
      endTime: new Date(end_date).getTime(),
    };
  }

  private isToday(date: string) {
    const todayUTCDate = new Date(
      Date.UTC(
        new Date().getUTCFullYear(),
        new Date().getUTCMonth(),
        new Date().getUTCDate(),
        new Date().getUTCHours(),
        new Date().getUTCMinutes(),
        new Date().getUTCSeconds(),
      ),
    )
      .toISOString()
      .split('T')[0];
    return date.startsWith(todayUTCDate);
  }

  private getDaysAfter(startDate: string) {
    const start = new Date(
      Date.UTC(
        new Date(startDate).getUTCFullYear(),
        new Date(startDate).getUTCMonth(),
        new Date(startDate).getUTCDate(),
        new Date(startDate).getUTCHours(),
        new Date(startDate).getUTCMinutes(),
        new Date(startDate).getUTCSeconds(),
      ),
    );
    const now = new Date(
      Date.UTC(
        new Date().getUTCFullYear(),
        new Date().getUTCMonth(),
        new Date().getUTCDate(),
        new Date().getUTCHours(),
        new Date().getUTCMinutes(),
        new Date().getUTCSeconds(),
      ),
    );
    const diff = now.getTime() - start.getTime();
    const days = diff / (1000 * 3600 * 24);
    return Math.floor(days);
  }

  private async getContributionsTable() {
    const response = await this._rpc.get_table_rows({
      json: true,
      code: 'daofnd.libre',
      scope: 'daofnd.libre',
      table: 'contribution',
      limit: 1000,
      reverse: false,
      show_payer: false,
    });
    const { rows } = response;
    return rows;
  }

  private async getContributionStats() {
    const response = await this._rpc.get_table_rows({
      json: true,
      code: 'daofnd.libre',
      scope: 'daofnd.libre',
      table: 'global',
      limit: 1,
      reverse: false,
      show_payer: false,
    });
    const { rows } = response;
    return rows[0];
  }

  private btcToSatoshi(amount: string) {
    const btc = Number(amount.replace(CHAIN_CONFIG().btc.symbol, '').trim());
    const satoshi = btc * 100000000;
    return satoshi;
  }

  private libreStringToNumber(amount: string) {
    const libre = Number(
      amount.replace(CHAIN_CONFIG().libre.symbol, '').trim(),
    );
    return libre;
  }
}
