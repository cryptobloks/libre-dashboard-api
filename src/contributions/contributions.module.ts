import { Module } from '@nestjs/common';
import { ContributionsService } from './contributions.service';
import { ContributionsController } from './contributions.controller';
import { HttpModule } from '@nestjs/axios';
import { TokensModule } from 'src/tokens/tokens.module';

@Module({
  imports: [HttpModule, TokensModule],
  controllers: [ContributionsController],
  providers: [ContributionsService],
})
export class ContributionsModule {}
