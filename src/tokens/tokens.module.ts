import { CacheModule, Module } from '@nestjs/common';
import { TokensService } from './tokens.service';
import { TokensController } from './tokens.controller';
import { StakesModule } from 'src/stakes/stakes.module';
import { HttpModule } from '@nestjs/axios';
import { CacheService as InternalCacheService } from '../cache/cache.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Stake } from 'src/cache/entities/stake.entity';
import { Turnover } from 'src/cache/entities/turnover.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Stake, Turnover]),
    HttpModule,
    StakesModule,
    CacheModule.register(),
  ],
  controllers: [TokensController],
  providers: [TokensService, InternalCacheService],
  exports: [TokensService],
})
export class TokensModule {}
