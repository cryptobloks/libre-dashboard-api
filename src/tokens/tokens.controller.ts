import {
  CacheInterceptor,
  CacheTTL,
  CACHE_MANAGER,
  Controller,
  Get,
  Inject,
  Param,
  UseInterceptors,
} from '@nestjs/common';
import { CacheService } from '../cache/cache.service';
import { TokensService } from './tokens.service';
import { CHAIN_CONFIG } from '../core/config';

@Controller(`${CHAIN_CONFIG().baseUri.server}tokens`)
export class TokensController {
  constructor(
    private readonly tokensService: TokensService,
    private readonly cacheService: CacheService,
    @Inject(CACHE_MANAGER) protected readonly cacheManager,
  ) {}

  private tokenSettings = {
    libre: {
      icon: 'https://cdn.libre.org/icon-libre-v2.svg',
      precision: 4,
    },
    pusdt: {
      icon: 'https://cdn.libre.org/icon-usdt-v2.svg',
      precision: 9,
    },
    pbtc: {
      icon: 'https://cdn.libre.org/icon-btc.svg',
      precision: 9,
    },
    btcl: {
      icon: 'https://cdn.libre.org/icon-btc.svg',
      precision: 8,
    },
    usdl: {
      icon: 'https://cdn.libre.org/icon-usdt-v2.svg',
      precision: 6,
    },
  };

  @Get()
  @CacheTTL(10)
  @UseInterceptors(CacheInterceptor)
  async getAllTokens() {
    const [
      pbtcStats,
      usdtStats,
      swapBalances,
      daoBalances,
      libreStats,
      btcPrice,
      librePrice,
    ] = await Promise.all([
      this.tokensService.getCurrencyStats(
        CHAIN_CONFIG().btc.contract,
        CHAIN_CONFIG().btc.symbol,
      ),
      this.tokensService.getCurrencyStats(
        CHAIN_CONFIG().usdt.contract,
        CHAIN_CONFIG().usdt.symbol,
      ),
      this.tokensService.getTokensByAccount('swap.libre'),
      this.tokensService.getTokensByAccount('dao.libre'),
      this.tokensService.getCurrencyStats(
        CHAIN_CONFIG().libre.contract,
        CHAIN_CONFIG().libre.symbol,
      ),
      this.tokensService.getBTCPrice(),
      this.tokensService.getLibrePrice(),
    ]);

    const daoLibreBalance = daoBalances.find((c) => c.name === 'LIBRE').total;
    const swapLibreBalance = swapBalances.find((c) => c.name === 'LIBRE').total;
    const mintRushAllocation = 200000000;

    const stakesCached = await (
      await this.cacheService.findAllStakes()
    ).filter((stake) => stake.status.toString() === '1');

    const libreStaked =
      swapLibreBalance +
      stakesCached.reduce(
        (partialSum, stake) =>
          partialSum + Number(stake.libre_staked.split(' LIBRE')[0]),
        0,
      );

    const libreSupply = libreStats + mintRushAllocation - daoLibreBalance;
    const freshResult = [
      {
        name: CHAIN_CONFIG().btc.symbol,
        symbol: CHAIN_CONFIG().btc.symbol,
        supply: pbtcStats,
        marketCap: pbtcStats * btcPrice,
        icon: this.tokenSettings[CHAIN_CONFIG().btc.symbol.toLowerCase()].icon,
        precision:
          this.tokenSettings[CHAIN_CONFIG().btc.symbol.toLowerCase()].precision,
        staked: 0,
        apy: null,
      },
      {
        name: CHAIN_CONFIG().usdt.symbol,
        symbol: CHAIN_CONFIG().usdt.symbol,
        supply: usdtStats,
        marketCap: usdtStats,
        icon: this.tokenSettings[CHAIN_CONFIG().usdt.symbol.toLowerCase()].icon,
        precision:
          this.tokenSettings[CHAIN_CONFIG().usdt.symbol.toLowerCase()]
            .precision,
        staked: 0,
        apy: null,
      },
      {
        name: CHAIN_CONFIG().libre.symbol,
        symbol: CHAIN_CONFIG().libre.symbol,
        supply: libreSupply,
        marketCap: libreSupply * librePrice,
        icon: this.tokenSettings[CHAIN_CONFIG().libre.symbol.toLowerCase()]
          .icon,
        precision:
          this.tokenSettings[CHAIN_CONFIG().libre.symbol.toLowerCase()]
            .precision,
        staked: (libreStaked / libreSupply) * 100,
        apy: '20% - 300%',
      },
    ];
    console.log('Invoked!!');
    return freshResult;
  }

  @Get(':account')
  getByAccount(@Param('account') account: string) {
    return this.tokensService.getTokensByAccount(account);
  }
}
