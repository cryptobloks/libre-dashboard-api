import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { JsonRpc } from 'eosjs';
import { CHAIN_CONFIG } from '../core/config';
import { StakesService } from 'src/stakes/stakes.service';
import { CacheService } from '../cache/cache.service';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const fetch = require('node-fetch');

@Injectable()
export class TokensService {
  private _rpc;

  constructor(
    private _httpService: HttpService,
    private _stakeService: StakesService,
    private _cacheService: CacheService,
  ) {
    this._rpc = new JsonRpc(CHAIN_CONFIG().hyperionAPI.url, { fetch });
  }

  private tokenSettings = {
    libre: {
      icon: 'https://cdn.libre.org/icon-libre-v2.svg',
      precision: 4,
    },
    pusdt: {
      icon: 'https://cdn.libre.org/icon-usdt-v2.svg',
      precision: 9,
    },
    pbtc: {
      icon: 'https://cdn.libre.org/icon-btc.svg',
      precision: 9,
    },
    btcl: {
      icon: 'https://cdn.libre.org/icon-btc.svg',
      precision: 8,
    },
    usdl: {
      icon: 'https://cdn.libre.org/icon-usdt-v2.svg',
      precision: 6,
    },
  };

  async getPoolStats(pool: string) {
    const response = await this._rpc.get_table_rows({
      json: true, // Get the response as json
      code: 'swap.libre', // Contract that we target
      scope: pool.toUpperCase(), // Account that owns the data
      table: 'stat', // Table name
      limit: 10, // Maximum number of rows that we want to get
      reverse: false, // Optional: Get reversed data
      show_payer: false, // Optional: Show ram payer
    });
    const row = response.rows[0];
    const pool1Amount = Number(row.pool1.quantity.split(' ')[0]);
    const pool2Amount = Number(row.pool2.quantity.split(' ')[0]);
    return {
      pool1Amount,
      pool2Amount,
    };
  }

  async getBTCUSDRate() {
    const response = await this._rpc.get_table_rows({
      json: true, // Get the response as json
      code: 'swap.libre', // Contract that we target
      scope: 'BTCUSD', // Account that owns the data
      table: 'stat', // Table name
      limit: 10, // Maximum number of rows that we want to get
      reverse: false, // Optional: Get reversed data
      show_payer: false, // Optional: Show ram payer
    });
    const row = response.rows[0];
    const btcAmount = Number(
      row.pool1.quantity.replace(CHAIN_CONFIG().btc.symbol, ''),
    );
    const usdtAmount = Number(
      row.pool2.quantity.replace(CHAIN_CONFIG().usdt.symbol, ''),
    );
    const price = Number((usdtAmount / btcAmount).toFixed(2));
    return price;
  }

  async getLibreBTCRate() {
    const response = await this._rpc.get_table_rows({
      json: true, // Get the response as json
      code: 'swap.libre', // Contract that we target
      scope: 'BTCLIB', // Account that owns the data
      table: 'stat', // Table name
      limit: 10, // Maximum number of rows that we want to get
      reverse: false, // Optional: Get reversed data
      show_payer: false, // Optional: Show ram payer
    });
    const row = response.rows[0];
    const btcAmount = Number(
      row.pool1.quantity.replace(CHAIN_CONFIG().btc.symbol, ''),
    );
    const libreAmount = Number(
      row.pool2.quantity.replace(CHAIN_CONFIG().libre.symbol, ''),
    );
    const price = Number((btcAmount / libreAmount).toFixed(16));
    return price;
  }

  async getMintRushStats() {
    const response = await this._rpc.get_table_rows({
      json: true,
      code: 'stake.libre',
      table: 'mintstats',
      scope: 'stake.libre',
    });
    if (!response || !response.rows || !response.rows.length) {
      return 0;
    }
    return Number(response.rows[0].btc_contributed.split(' ')[0]);
  }

  async getLibrePrice() {
    const [priceInSats, btcPrice] = await Promise.all([
      this.getLibreBTCRate(),
      this.getBTCUSDRate(),
    ]);

    return priceInSats * btcPrice;
  }

  async getBTCPrice() {
    return this.getBTCUSDRate();
  }

  async getTokensByAccount(account: string) {
    const data = await this._httpService
      .get(
        `${
          CHAIN_CONFIG().hyperionAPI.url
        }/v2/state/get_tokens?account=${account}`,
      )
      .toPromise();
    let tokens = data.data.tokens || [];
    tokens = tokens.filter(
      (c) =>
        c.contract === 'eosio.token' ||
        c.contract === 'btc.ptokens' ||
        c.contract === 'usdt.ptokens',
    );
    const result = [];
    const staked = await this._cacheService.findStakesByAccount(account);
    const activeStakes = staked
      .filter((c) => c.status == 1)
      .map((c) => c.libre_staked)
      .map((c) => Number(c.replace('LIBRE', '')))
      .reduce((a, b) => a + b, 0);
    for (const token of tokens) {
      if (token.symbol === 'LIBRE') {
        result.push({
          name: 'LIBRE',
          symbol: 'LIBRE',
          total: token.amount + activeStakes,
          staked: activeStakes,
          unstaked: token.amount,
          apy: '10-200%',
          enabled: true,
          icon: this.tokenSettings.libre.icon,
          precision: this.tokenSettings.libre.precision,
        });
      } else {
        result.push({
          name: token.symbol,
          symbol: token.symbol,
          total: token.amount,
          staked: 0,
          unstaked: token.amount,
          apy: null,
          enabled: true,
          icon: this.tokenSettings[token.symbol.toLowerCase()].icon,
          precision: this.tokenSettings[token.symbol.toLowerCase()].precision,
        });
      }
    }
    const tokenList = [
      CHAIN_CONFIG().btc.symbol,
      CHAIN_CONFIG().usdt.symbol,
      CHAIN_CONFIG().libre.symbol,
    ];
    for (const item of tokenList) {
      const resultHasToken = result.filter((c) => c.symbol === item).length > 0;
      if (!resultHasToken) {
        result.push({
          name: item,
          symbol: item,
          total: 0,
          staked: 0,
          unstaked: 0,
          apy: null,
          enabled: true,
          icon: this.tokenSettings[item.toLowerCase()].icon,
          precision: this.tokenSettings[item.toLowerCase()].precision,
        });
      }
    }
    const sorted = [];
    for (const item of tokenList) {
      const token = result.filter((c) => c.symbol === item)[0];
      sorted.push(token);
    }
    return sorted;
  }

  async getCurrencyStats(contract: string, token: string) {
    try {
      try {
        const data = await this._httpService
          .post(
            `${CHAIN_CONFIG().hyperionAPI.url}/v1/chain/get_currency_stats`,
            {
              code: contract,
              symbol: token,
            },
          )
          .toPromise();
        return Number(data.data[token].supply.replace(token, ''));
      } catch (e) {
        const data = await this._httpService
          .post(
            `${
              CHAIN_CONFIG().hyperionAPI.backup_url
            }/v1/chain/get_currency_stats`,
            {
              code: contract,
              symbol: token,
            },
          )
          .toPromise();
        return Number(data.data[token].supply.replace(token, ''));
      }
    } catch (e) {
      return;
    }
  }

  async getBalanceRemainingForSpindrop() {
    const data = await this._httpService
      .get(
        `${
          CHAIN_CONFIG().hyperionAPI.url
        }/v2/state/get_tokens?account=drop.libre`,
      )
      .toPromise();
    const tokens = data.data.tokens.filter((c) => c.symbol == 'LIBRE');
    if (tokens && tokens.length) {
      return tokens[0].amount;
    } else {
      return 0;
    }
  }
}
