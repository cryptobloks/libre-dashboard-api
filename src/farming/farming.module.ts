import { CacheModule, Module } from '@nestjs/common';
import { FarmingService } from './farming.service';
import { FarmingController } from './farming.controller';
import { HttpModule } from '@nestjs/axios';
import { MintsModule } from 'src/mints/mints.module';
import { TokensModule } from 'src/tokens/tokens.module';

@Module({
  imports: [HttpModule, MintsModule, TokensModule, CacheModule.register()],
  providers: [FarmingService],
  controllers: [FarmingController],
  exports: [FarmingService],
})
export class FarmingModule {}
