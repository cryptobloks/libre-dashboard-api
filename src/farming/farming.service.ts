import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { JsonRpc } from 'eosjs';
import { CHAIN_CONFIG } from '../core/config';
import { MintsService } from 'src/mints/mints.service';
import { TokensService } from 'src/tokens/tokens.service';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const fetch = require('node-fetch');

@Injectable()
export class FarmingService {
  private _rpc;

  constructor(
    private readonly _httpService: HttpService,
    private readonly _tokensService: TokensService,
  ) {
    this._rpc = new JsonRpc(CHAIN_CONFIG().hyperionAPI.url, { fetch });
  }

  async getRewardSettings() {
    const result = await this._httpService
      .post(`${CHAIN_CONFIG().hyperionAPI.url}/v1/chain/get_table_rows`, {
        code: 'reward.libre',
        table: 'global',
        scope: 'reward.libre',
        json: true,
      })
      .toPromise();
    if (!result || !result.data || !result.data.rows) {
      throw new Error('Error fetching reward settings');
    }
    return result.data.rows[0][1];
  }

  async getAvailablePools() {
    const result = await this._httpService
      .post(`${CHAIN_CONFIG().hyperionAPI.url}/v1/chain/get_table_rows`, {
        code: 'reward.libre',
        table: 'global',
        scope: 'reward.libre',
        json: true,
      })
      .toPromise();
    if (
      !result ||
      !result.data ||
      !result.data.rows ||
      !result.data.rows[0] ||
      !result.data.rows[0][1]
    )
      return [];
    return result.data.rows[0][1].allowed_pools;
  }

  async getPoolStats(symbol) {
    const response = await this._rpc.get_table_rows({
      json: true, // Get the response as json
      code: 'swap.libre', // Contract that we target
      scope: symbol, // Account that owns the data
      table: 'stat', // Table name
      limit: 10, // Maximum number of rows that we want to get
      reverse: false, // Optional: Get reversed data
      show_payer: false, // Optional: Show ram payer
    });
    const row: any = response.rows[0];
    const pool1Amount = Number(
      row.pool1.quantity.replace(` ${CHAIN_CONFIG().btc.symbol}`, ''),
    );
    const pool2Amount = Number(
      row.pool2.quantity.replace(` ${CHAIN_CONFIG().usdt.symbol}`, ''),
    );
    const price = Number((pool2Amount / pool1Amount).toFixed(2));
    return {
      pool1Amount,
      pool2Amount,
      price,
      supply: Number(row.supply.replace(symbol, '')),
    };
  }

  async getBTCUSDFarmingStats() {
    const precisionAdjustment = 10000;
    const satsPerBTC = 100000000;
    const blocksPerDay = 172800;
    const daysInYear = 365;
    const [rewardSettings, btcUsdPoolStats, lpBalance] = await Promise.all([
      this.getRewardSettings(),
      this.getPoolStats('BTCUSD'),
      this.getLPBalance('BTCUSD'),
    ]);

    const allowedPoolCount = rewardSettings.allowed_pools.length;
    const rewardPerBlock = rewardSettings.reward_per_block;
    const rewardPerFarm =
      rewardPerBlock / precisionAdjustment / allowedPoolCount;

    const lpTokenValue =
      (btcUsdPoolStats.pool2Amount * 2) / btcUsdPoolStats.supply;

    const libreUSDValue = await this._tokensService.getLibrePrice();
    const totalFarmValue = lpBalance * lpTokenValue;

    const apy = Number(
      (
        (100 * libreUSDValue * blocksPerDay * rewardPerFarm * daysInYear) /
        totalFarmValue
      ).toFixed(2),
    );

    return {
      symbol: 'BTCUSD',
      total_staked: 0,
      farming_staked: lpBalance ?? 0,
      apy,
      reward_per_farm: rewardPerFarm,
      lpTokenValue,
    };
    /*
[
  {"symbol":"BTCUSD",
  "total_staked":0,
  "farming_staked":"628.837523019",
  "apy":198.5807757652758,
  "reward_per_farm":1}
]
*/
  }

  async getBTCLIBFarmingStats() {
    const precisionAdjustment = 10000;
    const satsPerBTC = 100000000;
    const blocksPerDay = 172800;
    const daysInYear = 365;

    const [rewardSettings, btcPrice, btcLibPoolStats, lpBalance] =
      await Promise.all([
        this.getRewardSettings(),
        this._tokensService.getBTCPrice(),
        this.getPoolStats('BTCLIB'),
        this.getLPBalance('BTCLIB'),
      ]);

    const allowedPoolCount = rewardSettings.allowed_pools.length;
    const rewardPerBlock = rewardSettings.reward_per_block;
    const rewardPerFarm =
      rewardPerBlock / precisionAdjustment / allowedPoolCount;

    const lpTokenValue =
      (btcLibPoolStats.pool1Amount * 2 * btcPrice) / btcLibPoolStats.supply;

    const libreUSDValue = await this._tokensService.getLibrePrice();
    const totalFarmValue = lpBalance * lpTokenValue;

    const apy = Number(
      (
        (100 * libreUSDValue * blocksPerDay * rewardPerFarm * daysInYear) /
        totalFarmValue
      ).toFixed(2),
    );

    return {
      symbol: 'BTCLIB',
      total_staked: 0,
      farming_staked: lpBalance ?? 0,
      apy,
      reward_per_farm: rewardPerFarm,
      lpTokenValue,
    };
    /*
[
  {"symbol":"BTCUSD",
  "total_staked":0,
  "farming_staked":"628.837523019",
  "apy":198.5807757652758,
  "reward_per_farm":1}
]
*/
  }

  async getLPBalance(symbol: string): Promise<number> {
    const farmLPBalanceResult = await this._httpService
      .post(`${CHAIN_CONFIG().hyperionAPI.url}/v1/chain/get_currency_balance`, {
        code: 'swap.libre',
        account: 'farm.libre',
        symbol,
      })
      .toPromise();
    try {
      return Number(
        farmLPBalanceResult.data && farmLPBalanceResult.data.length
          ? farmLPBalanceResult.data[0].replace(symbol, '')
          : 0,
      );
    } catch (e) {
      return 0;
    }
  }

  async findAll() {
    const [btcUSDFarm, btcLIBFarm] = await Promise.all([
      this.getBTCUSDFarmingStats(),
      this.getBTCLIBFarmingStats(),
    ]);

    return [btcUSDFarm, btcLIBFarm];
  }

  async findByAccount(account: string) {
    let availablePools = [];
    let response = [];

    availablePools = await this.getAvailablePools();
    const precisions = {
      BTCUSD: 9,
      BTCLIB: 6,
    };
    for (const symbol of availablePools) {
      const result = await this._httpService
        .post(`${CHAIN_CONFIG().hyperionAPI.url}/v1/chain/get_table_rows`, {
          code: 'reward.libre',
          table: 'provider',
          scope: symbol,
          lower_bound: account,
          upper_bound: account,
          json: true,
        })
        .toPromise();

      if (
        result &&
        result.data &&
        result.data.rows &&
        result.data.rows[0] &&
        result.data.rows[0][1]
      ) {
        const data = result.data.rows[0][1];
        response = [
          ...response,
          {
            ...data,
            symbol,
            total_staked: Number(
              data['total_staked'] / Math.pow(10, precisions[symbol]),
            ).toFixed(precisions[symbol]),
            claimed: Number(data['claimed'] / Math.pow(10, 4)).toFixed(4),
            unclaimed: Number(data['unclaimed'] / Math.pow(10, 4)).toFixed(4),
          },
        ];
      } else {
        response = [
          ...response,
          {
            symbol,
            account: String(account),
            total_staked: Number(0).toFixed(0),
            claimed: Number(0).toFixed(4),
            unclaimed: Number(0).toFixed(4),
            last_claim: '',
          },
        ];
      }
    }

    return response;
  }
}
