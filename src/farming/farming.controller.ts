import {
  CacheInterceptor,
  CacheTTL,
  Controller,
  Get,
  Param,
  UseInterceptors,
} from '@nestjs/common';
import { FarmingService } from './farming.service';
import { CHAIN_CONFIG } from '../core/config';

@Controller(`${CHAIN_CONFIG().baseUri.server}farming`)
export class FarmingController {
  constructor(private readonly farmingService: FarmingService) {}

  @CacheTTL(10)
  @UseInterceptors(CacheInterceptor)
  @Get('stats')
  getAll() {
    return this.farmingService.findAll();
  }

  @Get(':accountName')
  getByAccount(@Param('accountName') accountName: string) {
    return this.farmingService.findByAccount(accountName);
  }
}
