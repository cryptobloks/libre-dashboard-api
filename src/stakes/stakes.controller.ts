import { Controller, Get, Param } from '@nestjs/common';
import { StakesService } from './stakes.service';
import { CHAIN_CONFIG } from '../core/config';

@Controller(`${CHAIN_CONFIG().baseUri.server}stakes`)
export class StakesController {
  constructor(private readonly _stakeService: StakesService) {}

  @Get()
  getAll() {
    return this._stakeService.findAll();
  }

  @Get(':account/list')
  findByAccount(@Param('account') account: string) {
    return this._stakeService.findByAccount(account);
  }

  @Get(':account/stats')
  async getAggregate(@Param('account') account: string) {
    const data = await this._stakeService.findByAccount(account);
    const staked = data
      .map((c) => Number(c.libre_staked.replace('LIBRE', '')))
      .reduce((a, b) => a + b, 0);
    const balance = await this._stakeService.getBalance(account, 'LIBRE');
    return {
      staked,
      balance,
      earned: 0,
    };
  }
}
