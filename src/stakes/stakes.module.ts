import { Module } from '@nestjs/common';
import { StakesService } from './stakes.service';
import { StakesController } from './stakes.controller';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [HttpModule],
  providers: [StakesService],
  controllers: [StakesController],
  exports: [StakesService],
})
export class StakesModule {}
