import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { CHAIN_CONFIG } from '../core/config';

@Injectable()
export class LiquidityProvidersService {
  constructor(private readonly _httpService: HttpService) {}

  async findByAccount(account: string) {
    const result = await this._httpService
      .post(`${CHAIN_CONFIG().hyperionAPI.url}/v1/chain/get_table_rows`, {
        code: CHAIN_CONFIG().hyperionAPI.swapContract,
        table: 'accounts',
        scope: account,
        json: true,
      })
      .toPromise();
    if (result.data.rows.length) {
      return result.data.rows[0];
    }
    return { balance: 0 };
  }
}
