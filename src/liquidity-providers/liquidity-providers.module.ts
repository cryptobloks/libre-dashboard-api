import { Module } from '@nestjs/common';
import { LiquidityProvidersService } from './liquidity-providers.service';
import { LiquidityProvidersController } from './liquidity-providers.controller';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [HttpModule],
  controllers: [LiquidityProvidersController],
  providers: [LiquidityProvidersService],
})
export class LiquidityProvidersModule {}
