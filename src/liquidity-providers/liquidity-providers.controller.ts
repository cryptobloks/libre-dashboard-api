import { Controller, Get, Param } from '@nestjs/common';
import { LiquidityProvidersService } from './liquidity-providers.service';
import { CHAIN_CONFIG } from '../core/config';

@Controller(`${CHAIN_CONFIG().baseUri.server}liquidity-providers`)
export class LiquidityProvidersController {
  constructor(
    private readonly liquidityProvidersService: LiquidityProvidersService,
  ) {}

  @Get(':account/balance')
  async getBalance(@Param('account') account: string) {
    const result = await this.liquidityProvidersService.findByAccount(account);
    return result;
  }
}
