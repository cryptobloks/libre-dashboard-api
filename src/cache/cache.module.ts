import { Module } from '@nestjs/common';
import { CacheService } from './cache.service';
import { CacheController } from './cache.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Stake } from './entities/stake.entity';
import { StakesModule } from 'src/stakes/stakes.module';
import { ScheduleModule } from '@nestjs/schedule';
import { Turnover } from './entities/turnover.entity';
import { VolumesModule } from 'src/volumes/volumes.module';
import { ExchangeRatesModule } from 'src/exchange-rates/exchange-rates.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Stake, Turnover]),
    StakesModule,
    ScheduleModule,
  ],
  controllers: [CacheController],
  providers: [CacheService],
  exports: [CacheService],
})
export class CacheModule {}
