import {
  Entity,
  Column,
  OneToMany,
  ManyToOne,
  ManyToMany,
  Generated,
} from 'typeorm';
import { BaseEntity } from '../../core/entities/base.entity';

@Entity({ name: 'stakes' })
export class Stake extends BaseEntity {
  constructor(partial: Partial<Stake>) {
    super();
    Object.assign(this, partial);
  }

  @Column({ type: 'numeric', nullable: false })
  index: number;

  @Column({ type: 'varchar', length: 200, nullable: true })
  account: string;

  @Column({ type: 'varchar', length: 200, nullable: true })
  stake_date: string;

  @Column({ type: 'varchar', length: 200, nullable: true })
  stake_length: string;

  @Column({ type: 'varchar', length: 200, nullable: true })
  mint_bonus: string;

  @Column({ type: 'varchar', length: 200, nullable: true })
  libre_staked: string;

  @Column({ type: 'varchar', length: 200, nullable: true })
  apy: string;

  @Column({ type: 'varchar', length: 200, nullable: true })
  payout: string;

  @Column({ type: 'varchar', length: 200, nullable: true })
  payout_date: string;

  @Column({ type: 'numeric', nullable: false })
  status: number;
}
